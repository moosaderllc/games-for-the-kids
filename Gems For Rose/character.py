import pygame, sys, random
from pygame.locals import *

class Character:
    def __init__( self ):
        self.x, self.y = 0, 0
        self.w, self.h = 0, 0
        self.speed = 5
        self.image = None
        self.frame = None
        self.score = 0
        
    def Setup( self, image ):
        self.image = image
        
    def Draw( self, window ):
        if ( self.image is None ): return
        
        if ( self.frame is None ):
            window.blit( self.image, ( self.x, self.y ) )
        else:
            window.blit( self.image, ( self.x, self.y ), self.frame )
        
    def Randomize( self, minX, minY, maxX, maxY ):
        self.x = random.randint( minX, maxX )
        self.y = random.randint( minY, maxY )
        
        gemType = random.randint( 0, 2 )
        self.frame = ( gemType * 32, 0, 32, 32 )
        self.score = gemType * 2 + 1
    
    def SetPosition( self, x, y ):
        self.x = x - self.w / 2
        self.y = y - self.h / 2
    
    def SetDimensions( self, w, h ):
        self.w = w
        self.h = h
        
    def SetFrame( self, frame ):
        self.frame = frame
        
    def Move( self, direction ):
        if ( direction == "UP" ):
            self.y -= self.speed
        elif ( direction == "DOWN" ):
            self.y += self.speed
        elif ( direction == "LEFT" ):
            self.x -= self.speed
        elif ( direction == "RIGHT" ):
            self.x += self.speed
            
    def KeepInScreen( self, minX, minY, maxX, maxY ):
        if ( self.x < minX ):               self.x = minX
        elif ( self.x + self.w > maxX ):    self.x = maxX - self.w
        if ( self.y < minY ):               self.y = minY
        elif ( self.y + self.h > maxY ):    self.y = maxY - self.h
            
    def IncrementScore( self, amount ):
        self.score += amount
        
    def IsCollision( self, otherCharacter ):
        return ( self.x + self.w > otherCharacter.x and
                self.x < otherCharacter.x + otherCharacter.w and
                self.y + self.h > otherCharacter.y and
                self.y < otherCharacter.y + otherCharacter.h )
     
