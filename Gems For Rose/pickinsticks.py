import pygame, sys, random
from pygame.locals import *
from character import Character

SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 600

viewWidth = SCREEN_HEIGHT
viewHeight = SCREEN_HEIGHT
menuLeft = viewWidth
menuWidth = SCREEN_WIDTH - SCREEN_HEIGHT
menuHeight = SCREEN_HEIGHT

# Initialization
pygame.init()
fpsClock = pygame.time.Clock()

window = pygame.display.set_mode( ( SCREEN_WIDTH, SCREEN_HEIGHT ), FULLSCREEN )
#window = pygame.display.set_mode( ( SCREEN_WIDTH, SCREEN_HEIGHT ) )
pygame.display.set_caption( "Pickin' Sticks" )

blankColor = pygame.Color( 200, 100, 100 )
textColor = pygame.Color( 255, 255, 255 )

# Load images
grass = pygame.image.load( "content/background.png" )
sidepane = pygame.image.load( "content/sidepane.png" )
collectables = pygame.image.load( "content/gems.png" )
girlSurface = pygame.image.load( "content/rose.png" )

# Load fonts
font = pygame.font.Font( "content/Jellee-Roman.otf", 20 )
smallFont = pygame.font.Font( "content/Jellee-Roman.otf", 15 )
bigFont = pygame.font.Font( "content/Jellee-Roman.otf", 50 )

# Load sounds
music = pygame.mixer.music.load( "content/DancingBunnies_Moosader.ogg" )

# More setup
gem = Character()
gem.Setup( collectables )
gem.SetDimensions( 32, 32 )
gem.Randomize( 0, 0, viewWidth-20, viewHeight )

girl = Character()
girl.Setup( girlSurface )
girl.SetDimensions( 32, 48 )
girl.SetPosition( viewWidth/2, viewHeight/2 )
girl.SetFrame( ( 0, 0, 53, 93 ) )

instructionsSurface1 = font.render( "Gems For Rose", False, textColor )
instructionsSurface2 = smallFont.render( "By Rachel", False, textColor )
instructionsSurface3 = font.render( "Move with the arrow keys.", False, textColor )
instructionsSurface4 = font.render( "Collect the gems.", False, textColor )
instructionsSurface5 = font.render( "ESC to quit.", False, textColor )
score = bigFont.render( "Score: " + str( girl.score ), False, textColor )

pygame.mixer.music.play()

# Game loop
while True:
    window.fill( blankColor )
    
    window.blit( grass, ( 0, 0 ) )
    window.blit( sidepane, ( menuLeft, 0 ) )
        
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
            
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.event.post( pygame.event.Event( QUIT ) )
                
    # Keyboard input
    keys = pygame.key.get_pressed()
    if ( keys[ K_UP ] ): girl.Move( "UP" )
    elif ( keys[ K_DOWN ] ): girl.Move( "DOWN" )
    if ( keys[ K_LEFT ] ): girl.Move( "LEFT" )
    elif ( keys[ K_RIGHT ] ): girl.Move( "RIGHT" )
    
    girl.KeepInScreen( 0, 0, viewWidth-20, viewHeight )
    
    # Collision detection
    if ( girl.IsCollision( gem ) ):
        girl.IncrementScore( gem.score )
        score = bigFont.render( "Score: " + str( girl.score ), False, textColor )
        gem.Randomize( 0, 0, viewWidth-20, viewHeight )
    
    # Draw characters and text
    gem.Draw( window )
    girl.Draw( window )
                
    window.blit( instructionsSurface1, ( menuLeft + 100, 75 ) )
    window.blit( instructionsSurface2, ( menuLeft + 100, 100 ) )
    window.blit( instructionsSurface3, ( menuLeft + 100, 200 ) )
    window.blit( instructionsSurface4, ( menuLeft + 100, 225 ) )
    window.blit( instructionsSurface5, ( menuLeft + 100, 250 ) )
    window.blit( score, ( menuLeft + 100, 350 ) )
    
    # Update screen & regulate framerate
    pygame.display.update()
    fpsClock.tick( 30 )
